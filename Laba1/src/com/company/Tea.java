package com.company;


import java.util.Scanner;
import java.util.UUID;

public class Tea extends Wood {

    private int packaging;

    Tea(UUID id) {
        super(id);
    }

    public Tea(String name, String country, String firma, int price, int packaging) {
        super(name, country, firma, price);
        this.packaging=packaging;

    }

    public Tea(){
        super();
        packaging = 0;
    }

    public Tea(Tea ob){
        super(ob);
        packaging=ob.packaging;
    }

    @Override
    public void create(){
        super.create();
        packaging = (int)(Math.random()*10);}

    @Override
    public void read(){
        super.read();
        System.out.println("Вид упаковки:" + packaging);

    }

    @Override
    public void update(){
        super.update();
        Scanner ss = new Scanner(System.in);
        System.out.println("Вид упаковки:");
        this.packaging = ss.nextInt();
    }

    @Override
    public void delete(){
        super.delete();
        this.packaging = 0;

    }

}
