package com.company;

import java.util.Scanner;
import java.util.UUID;

public abstract class Wood implements ICrudAction{
    static private int count;

    UUID id;
    Wood(UUID id){this.id= id;}

    private String name;
    private String country;
    private String firma;
    private int price;


    @Override
    public void create(){
        count++;

        String[] str = {"Lala", "La", "LL"};
        this.country = str[(int)(Math.random()*str.length)];

        this.name = str[(int)(Math.random()*str.length)];
        this.firma = str[(int)(Math.random()*str.length)];
        this.price = (int)(Math.random()*10);
    }

    @Override
    public void read(){
        System.out.println("Название товара:" +count);
        System.out.println("Название товара:" +this.name);
        System.out.println("Страна производитель:" +this.country);
        System.out.println("Фирма поставщик:" +this.firma);
        System.out.println("Цена:" +this.price);
    }

    @Override
    public void update(){

        count++;
        Scanner in1 = new Scanner(System.in);
        System.out.println("Назвние товара:");
        this.name = in1.nextLine();

        System.out.println("Страна производитель:");
        this.country = in1.nextLine();

        System.out.println("Фирма поставщик:");
        this.firma = in1.nextLine();

        System.out.println("Цена:");
        this.price = in1.nextInt();




    }


    @Override
    public void delete(){
        count--;
        this.name = "NULL";
        this.country = "NULL";
        this.firma = "NULL";
        this.price = 0;
    }

    public Wood (String name, String country, String firma, int price){
        this.country = country;
        this.name = name;
        this.firma = firma;
        this.price = price;
    }

    public Wood(Wood ob){
        this.name=ob.name;
        this.country=ob.country;
        this.firma=ob.firma;
        this.price=ob.price;
    }

   public Wood (){
        this.country = "Russia";
        this.name = "SS";
        this.firma = "DD";
        this.price = 100;
    }


}
