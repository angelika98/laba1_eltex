package com.company;

import java.util.Scanner;
import java.util.UUID;

public class Coffee extends Wood {

    Coffee(UUID id){super(id);}

    private String ZERNA[] = {"Arabika", "Robusta"};

    private int zerno;

    @Override
    public void create()
    {super.create();
    zerno = (int)(Math.random()*ZERNA.length);}

    @Override
    public void read(){

        super.read();
        System.out.println("Зерно:" +ZERNA[zerno]);
    }

    @Override
    public void update(){
        super.update();
        Scanner sc = new Scanner(System.in);
        System.out.println("Вид зерна [0-1]:");
        this.zerno = sc.nextInt();
        }

    @Override
    public void delete(){
        super.delete();
        this.zerno = 0;
    }


    public Coffee (String name, String country, String firma, int price, int zerno){
        super(name, country, firma, price);
        this.zerno=zerno;
    }



    public Coffee(Coffee ob){
        super(ob);
        zerno=ob.zerno;
    }

    public Coffee(){
        super();
        zerno=1;
    }


}
